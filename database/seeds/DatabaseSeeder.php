<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate();
        DB::table('user_notification_tokens')->truncate();
        DB::table('cron_logs')->truncate();
        DB::table('cron_schedules')->truncate();
        DB::table('cron_jobs')->truncate();
        DB::table('cron_bootstraps')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        factory(App\NotificationToken::class, 10)->create();
        factory(App\CronLog::class, 50)->create();
    }
}

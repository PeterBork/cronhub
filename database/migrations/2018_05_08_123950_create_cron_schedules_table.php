<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCronSchedulesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('cron_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('min', 16)->default('*');
            $table->string('hour', 16)->default('*');
            $table->string('day_of_week', 16)->default('*');
            $table->string('day_of_month', 16)->default('*');
            $table->string('month', 16)->default('*');
            $table->date('valid_from');
            $table->date('valid_to')->nullable();
            $table->string('restrict_type', 32)->nullable();
            $table->string('ib_type', 32)->nullable();
            $table->integer('cron_job_id')->unsigned();
            $table->foreign('cron_job_id')->references('id')->on('cron_jobs');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('cron_schedules');
    }
}

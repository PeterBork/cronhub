<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCronLogsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('cron_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->text('stdout')->nullable();
            $table->text('stderr')->nullable();
            $table->integer('exit_code')->nullable();
            $table->string('server_private_dns', 64);
            $table->string('restrict_type', 64);
            $table->string('ib_type', 64);
            $table->integer('cron_job_id')->unsigned();
            $table->foreign('cron_job_id')->references('id')->on('cron_jobs');
            $table->integer('cron_schedule_id')->unsigned();
            $table->foreign('cron_schedule_id')->references('id')->on('cron_schedules');
            $table->timestamps();
            $table->timestamp('finished_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('cron_logs');
    }
}

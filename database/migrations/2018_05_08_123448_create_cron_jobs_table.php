<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCronJobsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('cron_jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 128);
            $table->text('job');
            $table->text('description')->nullable();
            $table->integer('expected_running_time');
            $table->integer('cron_bootstrap_id')->unsigned();
            $table->foreign('cron_bootstrap_id')->references('id')->on('cron_bootstraps');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('cron_jobs');
    }
}

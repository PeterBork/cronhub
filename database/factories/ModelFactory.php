<?php

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\NotificationToken::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory(User::class)->create()->id;
        },
        'device' => $faker->colorName,
        'token' => $faker->text('152')
    ];
});

$factory->define(App\CronBootstrap::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});

$factory->define(App\CronJob::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'job' => 'echo "Damn"',
        'description' => $faker->paragraph,
        'expected_running_time' => 50,
        'cron_bootstrap_id' => function() {
            return factory(App\CronBootstrap::class)->create()->id;
        },
    ];
});

$factory->define(App\CronSchedule::class, function (Faker $faker) {
    return [
        'min' => '*',
        'hour' => '*',
        'day_of_week' => '*',
        'day_of_month' => '*',
        'month' => '*',
        'valid_from' => Carbon::now(),
        'cron_job_id' => function() {
            return factory(App\CronJob::class)->create()->id;
        },
    ];
});

$factory->define(App\CronLog::class, function (Faker $faker) {
    $schedule = factory(App\CronSchedule::class)->create();
    return [
        'stdout' => $faker->paragraph,
        'stderr' => '',
        'exit_code' => $faker->randomDigit(),
        'server_private_dns' => Carbon::now(),
        'restrict_type' => collect(['staging', 'production'])->random(),
        'ib_type' => collect(['cms', 'mailhub', 'serverless'])->random(),
        'cron_job_id' => $schedule->cron_job_id,
        'cron_schedule_id' => $schedule->id
    ];
});

<?php

namespace Tests\Feature;

use App\CronSchedule;
use App\Jobs\JobExecutor;
use App\NotificationToken;
use App\User;
use Illuminate\Support\Facades\Notification;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\PayloadNotification;
use LaravelFCM\Message\PayloadNotificationBuilder;
use Tests\TestCase;

class JobExecutorTest extends TestCase {
    /** @test */
    public function can_execute_a_job() {
        $schedule = factory(CronSchedule::class)->create();
        $schedule->job->update(['job' => 'php artisan test:success 0']);

        dispatch_now(new JobExecutor($schedule));

        $this->assertDatabaseHas('cron_logs', [
            'cron_schedule_id' => $schedule->id,
            'exit_code' => 0,
            'stdout' => "Hej\nHej\nHej\nHej\nHej\nHej\nHej\nHej\nHej\nHej\n"
        ]);
    }

    /** @test */
    public function can_handle_failing_jobs() {
        $schedule = factory(CronSchedule::class)->create();
        $schedule->job->update(['job' => 'thiscommanddoesnotexist']);

        dispatch_now(new JobExecutor($schedule));

        $this->assertDatabaseHas('cron_logs', [
            'cron_schedule_id' => $schedule->id,
            'exit_code' => 127,
            'stdout' => ''
        ]);
        $this->assertNotEmpty($schedule->logs->first()->stderr);

        $schedule = factory(CronSchedule::class)->create();
        $schedule->job->update(['job' => 'php artisan commandmissing']);

        dispatch_now(new JobExecutor($schedule));

        $this->assertDatabaseHas('cron_logs', [
            'cron_schedule_id' => $schedule->id,
            'exit_code' => 1,
        ]);
        $this->assertContains('Command "commandmissing" is not defined.', $schedule->logs->first()->stdout);
    }

    /** @test */
    public function can_send_notifications_when_job_fails() {
        Notification::fake();
        $user = factory(User::class)->create();
        $user->notificationSettings()->create(['firebase' => true, 'hipchat' => false]);
        $token = factory(NotificationToken::class)->create(['user_id' => $user->id]);

        $schedule = factory(CronSchedule::class)->create();
        $schedule->job->subscribers()->attach($user);
        $schedule->job->update(['job' => 'thiscommanddoesnotexist']);

        FCM::shouldReceive('sendTo')
            ->withArgs(function($tokens, $options, $payload) use ($token) {
                return in_array($token->token, $tokens);
            })
            ->once();
        dispatch_now(new JobExecutor($schedule));
    }
}

<?php

namespace Tests\Feature;

use App\NotificationToken;
use App\User;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class NotificationTokensControllerTest extends TestCase {
    /** @test */
    public function can_list_a_users_tokens() {
        $this->signIn();
        $ownToken = factory(NotificationToken::class)->create(['user_id' => $this->authenticatedUser->id]);
        $otherToken = factory(NotificationToken::class)->create();

        $this->json(
            'get',
            route('users.notification-tokens.index', $this->authenticatedUser)
        )
            ->assertJson([
                $ownToken->toArray()
            ])
            ->assertDontSee($otherToken->token);
    }

    /** @test */
    public function can_create_tokens() {
        $data = [
            'device' => 'Desktop',
            'token' => str_repeat('test', 38) // 152
        ];

        $this->signIn()
            ->json(
                'post',
                route('users.notification-tokens.store', $this->authenticatedUser),
                $data
            )
            ->assertStatus(201)
            ->assertJson($data);

        $this->assertDatabaseHas('user_notification_tokens', $data);
    }

    /** @test */
    public function can_update_tokens() {
        $this->signIn();
        $data = [
            'device' => 'Desktop',
            'token' => str_repeat('test', 38) // 152
        ];
        $token = factory(NotificationToken::class)->create(['user_id' => $this->authenticatedUser->id]);

        $this->signIn()
            ->json(
                'put',
                route('users.notification-tokens.update', [$this->authenticatedUser, $token]),
                $data
            )
            ->assertStatus(200)
            ->assertJson($data);

        $this->assertDatabaseHas('user_notification_tokens', $data);
    }

    /** @test */
    public function can_delete_tokens() {
        $this->signIn();
        $token = factory(NotificationToken::class)->create(['user_id' => $this->authenticatedUser->id]);

        $this->json(
            'delete',
            route('users.notification-tokens.destroy', [$this->authenticatedUser, $token])
        )
            ->assertStatus(200);

        $this->assertDatabaseMissing('user_notification_tokens', $token->toArray());
    }

    /** @test */
    public function can_not_manage_tokens_for_other_users() {
        $data = [
            'device' => 'Desktop',
            'token' => str_repeat('test', 38) // 152
        ];
        $user = factory(User::class)->create();
        $token = factory(NotificationToken::class)->create(['user_id' => $user->id]);

        $this->signIn()
            ->json(
                'post',
                route('users.notification-tokens.store', $user),
                $data
            )
            ->assertStatus(403);

        $this->json(
                'put',
                route('users.notification-tokens.update', [$user, $token]),
                $data
            )
            ->assertStatus(403);

        $this->json(
            'put',
            route('users.notification-tokens.destroy', [$user, $token]),
            $data
        )
            ->assertStatus(403);
    }
}

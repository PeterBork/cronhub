<?php

namespace Tests\Feature;

use App\CronJob;
use App\CronSchedule;
use Carbon\Carbon;
use Tests\TestCase;

class SchedulesControllerTest extends TestCase {
    /** @test */
    public function can_create_a_schedule() {
        $this->signIn();
        $input = factory(CronSchedule::class)->make();

        $this->json('POST', route('schedules.store'), $input->toArray())
            ->assertStatus(201)
            ->assertJson($input->toArray());

        $this->assertDatabaseHas('cron_schedules', collect($input->toArray())->except('valid_from')->toArray());
    }

    /** @test */
    public function can_update_a_schedule() {
        $this->signIn();
        $schedule = factory(CronSchedule::class)->create();
        $input = factory(CronSchedule::class)->make(['min' => '1', 'valid_to' => Carbon::now()]);

        $this->json('PUT', route('schedules.update', $schedule), $input->toArray())
            ->assertStatus(200)
            ->assertJson($input->toArray());

        $this->assertDatabaseHas('cron_schedules', [
            'min' => '1',
            'valid_to' => Carbon::now()->format('Y-m-d')
        ]);
    }

    /** @test */
    public function can_delete_a_schedule() {
        $this->signIn();
        $schedule = factory(CronSchedule::class)->create();

        $this->json('delete', route('schedules.destroy', $schedule))
            ->assertStatus(200);

        $this->assertDatabaseMissing('cron_schedules', ['id' => $schedule->id]);
    }
}

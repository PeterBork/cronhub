<?php

namespace Tests\Feature;

use App\CronJob;
use Tests\TestCase;

class JobsControllerTest extends TestCase {
    /** @test */
    public function can_create_a_job() {
        $this->signIn();
        $input = factory(CronJob::class)->make();

        $this->json('POST', route('jobs.store'), $input->toArray())
            ->assertStatus(201)
            ->assertJson($input->toArray());

        $this->assertDatabaseHas('cron_jobs', $input->toArray());
    }

    /** @test */
    public function can_update_a_job() {
        $this->signIn();
        $job = factory(CronJob::class)->create();
        $input = factory(CronJob::class)->make();

        $this->json('PUT', route('jobs.update', $job), $input->toArray())
            ->assertStatus(200)
            ->assertJson($input->toArray());

        $this->assertDatabaseHas('cron_jobs', $input->toArray());
    }

    /** @test */
    public function can_delete_a_job() {
        $this->signIn();
        $job = factory(CronJob::class)->create();

        $this->json('delete', route('jobs.destroy', $job))
            ->assertStatus(200);

        $this->assertDatabaseMissing('cron_jobs', ['id' => $job->id]);
    }
}

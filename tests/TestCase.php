<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase;

    /**
     * @var User
     */
    public $authenticatedUser;

    /**
     * Signs in
     * @return TestCase
     */
    public function signIn()
    {
        $this->authenticatedUser = factory(User::class)->create();

        $this->actingAs($this->authenticatedUser);

        return $this;
    }
}

<?php

namespace Tests\Feature;

use App\CronJob;
use App\CronSchedule;
use App\Jobs\JobExecutor;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;

class CronSchedulerTest extends TestCase {
    /** @test */
    public function can_dispatch_due_jobs() {
        Bus::fake();
        $schedules = [
            factory(CronSchedule::class)->create()->id,
            factory(CronSchedule::class)->create(['min' => 3])->id
        ];
        $scheduleNot = factory(CronSchedule::class)->create(['min' => 5]);
        Carbon::setTestNow(Carbon::create(null, null, null, null, 3));

        $this->artisan('cronhub:scheduler');

        Bus::assertDispatched(JobExecutor::class, 2);
        Bus::assertDispatched(JobExecutor::class, function ($job) use ($schedules) {
            return in_array($job->schedule->id, $schedules);
        });
        Bus::assertNotDispatched(JobExecutor::class, function ($job) use ($scheduleNot) {
            return $job->schedule->id == $scheduleNot->id;
        });
    }

    /** @test */
    public function can_only_dispatch_valid_jobs() {
        Bus::fake();
        Carbon::setTestNow(Carbon::create(null, null, null, null, 3));
        factory(CronSchedule::class)->create(['valid_from' => Carbon::now()->addDay()]);
        factory(CronSchedule::class)->create(['valid_to' => Carbon::now()->subDay()]);

        $this->artisan('cronhub:scheduler');

        Bus::assertDispatched(JobExecutor::class, 0);
    }
}

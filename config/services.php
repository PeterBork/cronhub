<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'fcm' => [
        'key' => 'AAAAD8wi0J8:APA91bEQjWPclpJkBuxxAV3GAmtw4fMovNWyRxCMlL4gKfYt6Wsj2O1Zb69gCwj5IbUgnRyik4o72O3kYVd8XPN9Qy-Zh8HIgGRVfHc0zULVV7jMsZjwN4AZJKlD-bxGe0dKciH3BbiX'
     ],

    'hipchat' => [
        'token' => env('HIPCHAT_TOKEN'),
        'room' => env('HIPCHAT_ROOM_ID'),
        'url' => env('HIPCHAT_URL')
    ],
];

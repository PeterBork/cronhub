/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


import 'quasar-framework/dist/umd/quasar.mat.css'
import Quasar, * as All from 'quasar-framework/dist/quasar.mat.esm'
import router from './router'
import PortalVue from 'portal-vue'

require('./bootstrap')
require('quasar-extras/roboto-font')


window.Vue = require('vue')

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.use(PortalVue)
Vue.use(Quasar, {
    theme: 'mat',
    directives: All, // Test only, not for production
    components: All, // Test only, not for production
    plugins: All
})

Vue.component('app', require('./components/App.vue'))
Vue.component('login', require('./components/Login.vue'))

const app = new Vue({
    el: '#app',
    router,
    components: { Quasar }
})

import Vue from 'vue'
import VueRouter from 'vue-router'

import Dashboard from './components/Dashboard/Dashboard'
import JobsList from './components/Jobs/JobsList'
import JobsShow from './components/Jobs/JobsShow'
import JobsForm from './components/Jobs/JobsForm'
import SchedulesList from './components/Schedules/SchedulesList'
import SchedulesForm from './components/Schedules/SchedulesForm'
import LogsList from './components/Logs/LogsList'
import LogsShow from './components/Logs/LogsShow'
import Profile from './components/Notification/Notification'

Vue.use(VueRouter)

export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Dashboard
        },
        {
            path: '/jobs',
            component: JobsList
        },
        {
            path: '/jobs/create',
            component: JobsForm
        },
        {
            path: '/jobs/:id',
            component: JobsShow,
            props: true
        },
        {
            path: '/jobs/:id/edit',
            component: JobsForm,
            props: true
        },
        {
            path: '/schedules',
            component: SchedulesList
        },
        {
            path: '/schedules/create',
            component: SchedulesForm,
            props: true
        },
        {
            path: '/schedules/:id/edit',
            component: SchedulesForm,
            props: true
        },
        {
            path: '/logs',
            component: LogsList
        },
        {
            path: '/logs/:id',
            component: LogsShow,
            props: true
        },
        {
            path: '/profile',
            component: Profile
        },
        {
            path: '*'
        }
    ]
})

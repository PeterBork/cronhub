<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Carbon;

/**
 * Class User
 * @package App
 * @property integer id
 * @property string name
 * @property string email
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class User extends Authenticatable {
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password',];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token',];

    public function notificationTokens() {
        return $this->hasMany(NotificationToken::class);
    }

	public function notificationSettings() {
		return $this->hasOne(NotificationSettings::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CronLog extends Model {
    protected $guarded = [];

    protected $dates = ['finished_at'];
    protected $softDelete = true;

    public function job() {
        return $this->belongsTo(CronJob::class, 'cron_job_id');
    }

    public function schedule() {
        return $this->belongsTo(CronSchedule::class, 'cron_schedule_id');
    }

    public function logs() {
        return $this->hasManyThrough(CronLog::class, CronSchedule::class);
    }
}

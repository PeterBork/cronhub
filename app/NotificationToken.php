<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class NotificationToken
 * @package App
 * @property integer id
 * @property string device
 * @property string token
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class NotificationToken extends Model
{
    protected $guarded = [];

    protected $table = 'user_notification_tokens';
}

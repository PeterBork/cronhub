<?php

namespace App\Console\Commands;

use App\CronSchedule;
use App\Jobs\JobExecutor;
use Cron\CronExpression;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class CronScheduler extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronhub:scheduler';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command which dispatches cronjob if they are set to run at current time';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $now = Carbon::now();
        $schedules = CronSchedule::whereActive()->get();

        foreach ($schedules as $schedule) {
            if (CronExpression::factory($schedule->expression())->isDue($now)) {
                dispatch(new JobExecutor($schedule));
            }
        }
    }
}

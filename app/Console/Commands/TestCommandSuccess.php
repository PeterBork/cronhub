<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TestCommandSuccess extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:success {sleep=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command used for tests';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        for ($i = 0; $i < 10; $i++) {
            $this->info('Hej');

            sleep($this->argument('sleep'));
        }
    }
}

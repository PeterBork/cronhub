<?php

namespace App\Events;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ModelBroadcaster implements ShouldBroadcast {
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Model
     */
    public $model;
    private $eventType;

    /**
     * Create a new event instance.
     *
     * @param Model $model
     * @param $eventType
     */
    public function __construct(Model $model, $eventType) {
        $this->model = $model;
        $this->eventType = $eventType;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return string
     */
    public function broadcastOn() {
        return strtolower((new \ReflectionClass($this->model))->getShortName());
    }

    public function broadcastAs() {
        $className = (new \ReflectionClass($this->model))->getShortName();

        return $className .  '.' . ucfirst($this->eventType);
    }
}

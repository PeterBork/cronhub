<?php

namespace App\Observers;

use App\CronLog;
use App\Events\ModelBroadcaster;

class CronLogObserver {
    public function created(CronLog $log) {
        event(new ModelBroadcaster($log, 'created'));
    }

    public function updated(CronLog $log) {
        event(new ModelBroadcaster($log, 'updated'));
    }
}

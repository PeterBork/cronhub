<?php

namespace App\Observers;

use App\CronLog;
use App\CronSchedule;
use App\Events\ModelBroadcaster;
use Illuminate\Support\Facades\Cache;

class CronScheduleObserver {
    public function created(CronSchedule $schedule) {
        Cache::flush();
    }

    public function updated(CronSchedule $schedule) {
        Cache::flush();
    }

    public function deleted(CronSchedule $schedule) {
        Cache::flush();
    }
}

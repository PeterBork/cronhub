<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CronJob extends Model {
    protected $guarded = [];
    protected $softDelete = true;

    public function logs() {
        return $this->hasMany(CronLog::class);
    }

    public function bootstrap() {
        return $this->belongsTo(CronBootstrap::class, 'cron_bootstrap_id');
    }

    public function schedules() {
        return $this->hasMany(CronSchedule::class);
    }

    public function subscribers() {
        return $this->belongsToMany(User::class, 'cron_job_subscribers')->withTimestamps();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationSettings extends Model {
	protected $table = "user_notification_settings";
	protected $casts = [
	    'hipchat' => 'boolean',
	    'firebase' => 'boolean'
    ];
	protected $guarded = [];

	public function user() {
		return $this->belongsTo(NotificationSettings::class);
	}
}

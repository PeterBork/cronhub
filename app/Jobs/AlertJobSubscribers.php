<?php

namespace App\Jobs;

use App\CronJob;
use App\CronLog;
use App\Notifications\CronJobFailed;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Notification;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\PayloadNotificationBuilder;

class AlertJobSubscribers implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var CronLog
     */
    private $log;

    /**
     * Create a new job instance.
     *
     * @param CronLog $log
     */
    public function __construct(CronLog $log) {
        $this->log = $log;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $notificationBuilder = (new PayloadNotificationBuilder('Job failed'))
            ->setBody($this->log->job->name . ' with exit code:' . $this->log->exit_code)
            ->setClickAction(url('logs/' . $this->log->id))
            ->build();
        $tokens = $this->log->job->subscribers->map(function ($user) {
            if (($user->notificationSettings->firebase ?? false) === false) {
                return [];
            }

            return $user->notificationTokens()->pluck('token');
        })->flatten()->toArray();

        if (count($tokens) > 0) {
            FCM::sendTo($tokens, null, $notificationBuilder);
        }
        Notification::route('hipchat', config('services.hipchat.room'))
            ->notify(new CronJobFailed($this->log));
    }
}

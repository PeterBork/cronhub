<?php

namespace App\Jobs;

use App\CronJob;
use App\CronSchedule;
use App\Events\CronLogUpdated;
use App\Notifications\CronJobFailed;
use App\Notifications\JobFailed;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Symfony\Component\Process\Process;

class JobExecutor implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $schedule;

    /**
     * Create a new job instance.
     *
     * @param CronSchedule $schedule
     */
    public function __construct(CronSchedule $schedule)
    {
        $this->schedule = $schedule;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $log = $this->schedule->job->logs()->create([
            'server_private_dns' => '1',
            'restrict_type' => '1',
            'ib_type' => '1',
            'cron_schedule_id' => $this->schedule->id
        ]);
        $process = new Process($this->schedule->job->job);

        $process->run(function($type, $data) use ($process, $log) {
            $field = $type == Process::OUT ? 'stdout' : 'stderr';
            $log->update([
                $field => $log->$field . $data
            ]);

            if ($log->fresh()->exit_code == 143) {
                $process->stop();
            }
        });

        $log->update([
            'stdout' => $process->getOutput(),
            'stderr' => $process->getErrorOutput(),
            'exit_code' => $process->getExitCode(),
            'finished_at' => Carbon::now()
        ]);

        if ($log->exit_code > 0) {
            dispatch(new AlertJobSubscribers($log));
        }
    }
}

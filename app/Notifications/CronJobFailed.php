<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\HipChat\Card;
use NotificationChannels\HipChat\CardAttribute;
use NotificationChannels\HipChat\CardFormats;
use NotificationChannels\HipChat\CardStyles;
use NotificationChannels\HipChat\HipChatChannel;
use NotificationChannels\HipChat\HipChatMessage;

class CronJobFailed extends Notification {
    use Queueable;

    private $log;

    public function __construct($log) {
        $this->log = $log;
    }

    private function buildMessage($log) {
        $job_name = $log->job->name;
        $schedule_id = $log->schedule->id;
        $exit_code = $log->exit_code;

        return $job_name . " (schedule id: " . $schedule_id . ") has failed, Exit_code: " . $exit_code;
    }

    public function via($notifiable) {
        return [HipChatChannel::class];
    }

    public function toHipChat($notifiable) {
        $subscribers = $this->log->job->subscribers->filter(function ($user) {
            return ($user->notificationSettings->hipchat ?? false) === true && $user->hipchat_username !== null;
        })->map(function($user) {
            return '@' . $user->hipchat_username;
        })->implode(', ');

        $card = Card::create('Job failed')
            ->activity($this->buildMessage($this->log))
            ->html($this->log->stderr)
            ->url('https://cronhub.test/logs/' . $this->log->id)
            ->cardFormat(CardFormats::MEDIUM);

        if ($subscribers !== '') {
            $card->addAttribute(new CardAttribute($subscribers, 'Subscribers'));
        }

        return HipChatMessage::create()
             ->text($subscribers !== '' ? $subscribers : 'No subscribers')
             ->notify()
             ->error()
             ->card($card);
    }
}

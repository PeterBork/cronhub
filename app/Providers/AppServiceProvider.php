<?php

namespace App\Providers;

use App\CronLog;
use App\CronSchedule;
use App\Observers\CronLogObserver;
use App\Observers\CronScheduleObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        Schema::defaultStringLength(191);

        CronLog::observe(CronLogObserver::class);
        CronSchedule::observe(CronScheduleObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class CronSchedule extends Model {
    protected $guarded = [];
    protected $dates = ['valid_from', 'valid_to'];
    protected $softDelete = true;

    public function job() {
        return $this->belongsTo(CronJob::class, 'cron_job_id');
    }

    public function logs() {
        return $this->hasMany(CronLog::class);
    }

    public function expression() {
        return implode(' ', [
            $this->min,
            $this->hour,
            $this->day_of_month,
            $this->month,
            $this->day_of_week,
        ]);
    }

    /**
     * Queries schedules where is active for given date
     * @param $query
     * @param Carbon|null $start If not specified Carbon::now() will be used
     * @param Carbon|null $end If not specified Carbon::now() will be used
     * @return Builder
     */
    public function scopewhereActive($query, $start = null, $end = null) {
        $start = $start ?? Carbon::now();
        $end = $end ?? Carbon::now();

        return $query->whereDate('valid_from', '<=', $start)
            ->where(function($query) use ($end) {
                $query->whereDate('valid_to', '>=', $end)
                    ->orWhereNull('valid_to');
            });
    }
}

<?php

namespace App\Http\Controllers;

use App\CronBootstrap;

class BootstrapsController extends Controller {
    public function index() {
        return CronBootstrap::all();
    }
}

<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\NotificationToken;
use App\User;
use Illuminate\Auth\Access\Gate;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class NotificationTokensController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function index(User $user) {
        return response()->json($user->notificationTokens);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, User $user) {
        $validated = $this->validate($request, [
            'device' => 'required',
            'token' => ['required', 'size:152', Rule::unique('user_notification_tokens', 'token')]
        ]);

        $token = $user->notificationTokens()->create($validated);

        return response()->json($token, 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $user
     * @param  \App\NotificationToken $notificationToken
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, User $user, NotificationToken $notificationToken) {
        $validated = $this->validate($request, [
            'device' => 'required',
            'token' => ['required', 'size:152', Rule::unique('user_notification_tokens', 'token')->ignore($notificationToken->id)]
        ]);

        $notificationToken->update($validated);

        return response()->json($notificationToken);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @param  \App\NotificationToken $notificationToken
     * @return void
     * @throws \Exception
     */
    public function destroy(User $user, NotificationToken $notificationToken) {
        $notificationToken->delete();
    }
}

<?php

namespace App\Http\Controllers;

use App\CronJob;
use App\CronLog;
use App\CronSchedule;
use App\Jobs\JobExecutor;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class JobsController extends Controller {

    public function index() {
        return CronJob::all();
    }

    public function show(Request $request, CronJob $job) {
        if ($request->get('relationships', false) == true) {
            $job->load(['bootstrap', 'schedules', 'subscribers']);
        }

        return $job;
    }

    public function store(Request $request) {
        $input = $this->validate($request, [
            'name' => 'required',
            'description' => 'present',
            'expected_running_time' => 'required|integer',
            'cron_bootstrap_id' => 'required|exists:cron_bootstraps,id',
            'job' => 'required'
        ]);

        return CronJob::create($input);
    }

    public function update(Request $request, CronJob $job) {
        $input = $this->validate($request, [
            'name' => 'required',
            'description' => 'present',
            'expected_running_time' => 'required|integer',
            'cron_bootstrap_id' => 'required|exists:cron_bootstraps,id',
            'job' => 'required'
        ]);

        $job->update($input);

        return $job;
    }

    public function destroy($id) {
        // could probably be optimized
        $job = CronJob::find($id);
        $schedules = CronSchedule::where('cron_job_id', $id)->get();
        foreach ($schedules as $schedule) {
            CronLog::where('cron_schedule_id', $schedule->id)->delete();
            $schedule->delete();
        }
        CronLog::where('cron_job_id', $id)->delete();

        $job->delete();
    }

    public function search(Request $request) {
        $per_page = $request->per_page ?? 10;
        $filter = $request->filter;
        $query = CronJob::with('bootstrap', 'schedules');

        $query->when(!empty($filter), function ($query) use ($filter) {
            $query->where('name', 'like', '%' . $filter . '%');
        });

        return $query->paginate($per_page);
    }

    public function runManually(CronJob $job) {
        $schedule = CronSchedule::create([
            "min" => 0,
            "hour" => 0,
            "day_of_week" => 0,
            "day_of_month" => 0,
            "month" => 0,
            "valid_from" => Carbon::now(),
            "valid_to" => Carbon::now(),
            "cron_job_id" => $job->id
        ]);

        dispatch(new JobExecutor($schedule));
    }
}

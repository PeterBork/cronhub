<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller {
    public function create() {
        return view('login');
    }

    public function store(Request $request) {
        $input = $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        if (!Auth::attempt($input, true)) {
            return response()->json(['message' => 'Wrong credentials'], 422);
        }

        return response()->json([
            'url' => session()->pull('url.intended', '/')
        ], 200);
    }

    public function destroy() {
        Auth::logout();

        return redirect()->to('/login');
    }
}

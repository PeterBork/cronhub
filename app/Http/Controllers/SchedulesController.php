<?php

namespace App\Http\Controllers;

use App\CronLog;
use App\CronSchedule;
use Carbon\Carbon;
use Cron\CronExpression;
use Illuminate\Http\Request;

class SchedulesController extends Controller {
    public function index() {
        return CronSchedule::all();
    }

    public function show(CronSchedule $schedule) {
        return $schedule;
    }

    public function store(Request $request) {
        $input = $this->validate($request, [
            'cron_job_id' => 'required|exists:cron_jobs,id',
            'min' => 'required',
            'hour' => 'required',
            'day_of_week' => 'required',
            'day_of_month' => 'required',
            'month' => 'required',
            'valid_from' => 'required|date',
            'valid_to' => 'sometimes|nullable|date',
        ]);

        return CronSchedule::create($input);
    }

    public function update(Request $request, CronSchedule $schedule) {
        $input = $this->validate($request, [
            'cron_job_id' => 'required|exists:cron_jobs,id',
            'min' => 'required',
            'hour' => 'required',
            'day_of_week' => 'required',
            'day_of_month' => 'required',
            'month' => 'required',
            'valid_from' => 'required|date',
            'valid_to' => 'sometimes|nullable|date',
        ]);

        $schedule->update($input);

        return $schedule;
    }

    public function destroy(CronSchedule $schedule) {
        $schedule->logs()->delete();
        $schedule->delete();
    }

    public function search(Request $request) {
        $per_page = $request->per_page ?? 10;
        $filter = $request->filter;
        $query = CronSchedule::with('job');

        $query->when(!empty($filter), function ($query) use ($filter) {
            $query->whereHas('job', function ($subquery) use ($filter) {
                $subquery->where('name', 'like', '%' . $filter . '%');
            })->orWhere('id', $filter);
        });

        $paginator = $query->paginate($per_page);
        $paginator->getCollection()->transform(function ($item) {
            $item->expression = $item->expression();

            return $item;
        });

        return $paginator;
    }
}

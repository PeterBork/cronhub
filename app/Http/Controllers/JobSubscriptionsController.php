<?php

namespace App\Http\Controllers;

use App\CronJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JobSubscriptionsController extends Controller {
    public function show(CronJob $job) {
        if (!$job->subscribers()->where('user_id', Auth::id())->exists()) {
            return response()->json('', 404);
        }

        return response()->json('', 200);
    }

    public function store(CronJob $job) {
        if (Auth::user()->notificationTokens()->count() === 0) {
            return response()->json([
                'message' => 'You have no devices added',
                422
            ]);
        }

        $job->subscribers()->attach(Auth::id());

        return response()->json('', 201);
    }

    public function destroy(CronJob $job) {
        $job->subscribers()->detach(Auth::id());

        return response()->json('', 200);
    }
}

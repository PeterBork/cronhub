<?php

namespace App\Http\Controllers;

use App\NotificationSettings;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationSettingsController extends Controller {

	public function store(Request $request) {
		Auth::user()->notificationSettings()->updateOrCreate(
			['user_id' => Auth::id()],
			[
				"firebase" => in_array("firebase", $request->settings),
			    "hipchat" => in_array("hipchat", $request->settings )
			]);
	}

	public function show($id) {
		return NotificationSettings::where('user_id', $id)->get()->transform(function($settings){
			$activeValues = [];
			if($settings->firebase){
				$activeValues[] = "firebase";
			}
			if($settings->hipchat){
				$activeValues[] = "hipchat";
			}
			return $activeValues;
		})->flatten()->toArray();
	}

}

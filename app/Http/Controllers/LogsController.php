<?php

namespace App\Http\Controllers;

use App\CronLog;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LogsController extends Controller {
    public function index() {
        return CronLog::all();
    }

    public function show(CronLog $log) {
        $log->load('job');

        return response()->json($log);
    }

    public function search(Request $request) {
        $filters = json_decode($request->filter);
        $jobs = $filters->jobs ?? [];
        $schedules = $filters->schedules ?? [];
        $start_date = $filters->startDateFilter ?? null;
        $end_date = $filters->endDateFilter ?? null;

        $per_page = $request->per_page ?? 10;
        $query = CronLog::with('job.bootstrap');

        $query->when(!empty($jobs), function ($query) use ($jobs) {
            $query->whereIn('cron_job_id', $jobs);
        });
        $query->when(!empty($schedules), function ($query) use ($schedules) {
            $query->whereIn('cron_schedule_id', $schedules);
        });
        $query->when(!is_null($start_date), function ($query) use ($start_date) {
            $query->where('created_at', '>=', Carbon::createFromTimestampMs($start_date, 'Europe/Copenhagen'));
        });
        $query->when(!is_null($end_date), function ($query) use ($end_date) {
            $query->where('created_at', '<=', Carbon::createFromTimestampMs($end_date, 'Europe/Copenhagen'));
        });

        return $query->latest()->paginate($per_page);
    }

    public function terminate(CronLog $log) {
        $log->update(['exit_code' => 143]);
    }
}

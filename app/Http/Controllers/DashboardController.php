<?php

namespace App\Http\Controllers;

use App\CronLog;
use App\CronSchedule;
use Carbon\Carbon;
use Cron\CronExpression;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class DashboardController extends Controller {
    public function schedule(Request $request) {
        $date = Carbon::parse($request->date);
        $view = $request->view ?? 'day';
        $start = $date->copy()->startOfDay();
        $end = $date->copy()->endOfDay();
        $jobs = [];

        if ($view == 'week') {
            $start->startOfWeek();
            $end->endOfWeek();
        }

        if ($end > Carbon::now()) {
            $jobs = $this->fetchUpcomingJobs($start, $end);
        }

        $logs = CronLog::with('job')
            ->whereNotNull('finished_at')
            ->where('created_at', '>', $start)
            ->latest()
            ->get()
            ->map(function($log) {
                return ['date' => $log->created_at->toDateTimeString(), 'log' => $log];
            });
        $sorted = $logs->toBase()->merge($jobs)
            ->sortBy(function($field) {
                return $field['date'];
            })
            ->values();

        if ($view == 'week') {
            return $sorted;
        } else {
            return $sorted->groupBy(function($field) {
                return Carbon::parse($field['date'])->hour;
            });
        }
    }

    public function currentlyRunning() {
        return CronLog::with('schedule', 'job')->whereNull('finished_at')->get();
    }

    public function upcoming() {
        $now = Carbon::now();

        $schedules = CronSchedule::with('job')
            ->whereDate('valid_from', '<=', $now)
            ->where(function ($query) use ($now) {
                $query->whereDate('valid_to', '>=', $now)->orWhereNull('valid_to');
            })
            ->limit(5)
            ->get();

        return $schedules->transform(function ($schedule) use ($now) {
            $schedule->next_run_time = CronExpression::factory($schedule->expression())->getNextRunDate($now);

            return $schedule;
        })->sortByDesc('next_run_time')->values()->take(5);
    }

    public function mostRecent() {
        return CronLog::with('job')
            ->whereNotNull('finished_at')
            ->latest('finished_at')
            ->limit(5)
            ->get();
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @return array
     */
    public function fetchUpcomingJobs($start, $end) {
        $jobs = [];
        $day = $start->copy();
        $now = Carbon::now();

        for ($i = 0; $i <= $start->diffInDays($end); $i++) {
            if ($now < $day->copy()->endOfDay()) {
                $new = Cache::remember('schedule-' . $day->toDateString(), 60 * 24 * 7, function() use($day) {
                    return $this->fetchUpcomingJobsForDay($day);
                });
                $filtered = array_filter($new, function($job) use ($now) {
                    return $now < $job['date'];
                });

                $jobs = array_merge($jobs, $filtered);
            }

            $day->addDay();
        }

        return $jobs;
    }

    /**
     * @param Carbon $date
     * @return array
     */
    private function fetchUpcomingJobsForDay($date) {
        $jobs = [];
        $end = $date->copy()->endOfDay();
        $schedules = CronSchedule::with('job')->whereActive($date, $end)->get();

        foreach ($schedules as $schedule) {
            $end = $date->copy()->endOfDay();
            $expression = CronExpression::factory($schedule->expression());
            $now = $expression->getNextRunDate($date->copy()->startOfDay()->subMinute(1));

            while ($now <= $end) {
                $jobs[] = ['date' => Carbon::parse($now)->toDateTimeString(), 'job' => $schedule->job];

                $now = $expression->getNextRunDate($now);
            }
        }

        return $jobs;
    }
}



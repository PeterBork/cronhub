<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CronBootstrap extends Model {
    protected $gaured = [];

    public function jobs() {
        return $this->hasMany(CronJob::class);
    }
}

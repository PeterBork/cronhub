<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Laravel\Horizon\Horizon;

Route::get('login', 'LoginController@create');
Route::post('login', 'LoginController@store')->name('login');
Route::get('logout', 'LoginController@destroy');

Route::view('{any}', 'app')
    ->name('base')
    ->where('any', '.*')
    ->middleware('auth');

Horizon::auth(function () {
    return Auth::check();
});

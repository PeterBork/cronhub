<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth'], function() {
    Route::group(['prefix' => 'users', 'as' => 'users.'], function() {
        Route::apiResource('{user}/notification-tokens', 'Users\NotificationTokensController')->middleware('can:manage,user');
    });

	/* jobs routes */
    Route::get('jobs/search', 'JobsController@search');
    Route::get('jobs/{job}/subscribers', 'JobSubscriptionsController@show')->name('jobs.subscriptions.show');
    Route::post('jobs/{job}/subscribers', 'JobSubscriptionsController@store')->name('jobs.subscriptions.store');
    Route::delete('jobs/{job}/subscribers', 'JobSubscriptionsController@destroy')->name('jobs.subscriptions.destroy');
    Route::post('jobs/{job}/run-manually', 'JobsController@runManually')->name('jobs.manually');
    Route::apiResource('jobs', 'JobsController');

    /* bootstraps routes */
    Route::apiResource('bootstraps', 'BootstrapsController');

    /* schedules routes */
    Route::get('schedules/search', 'SchedulesController@search');
    Route::apiResource('schedules', 'SchedulesController');

    /* logs routes */
    Route::get('logs/search', 'LogsController@search')->name('logs.search');
    Route::post('logs/{log}/terminate', 'LogsController@terminate')->name('logs.terminate');
    Route::apiResource('logs', 'LogsController');

    /* dashboard routes */
    Route::get('dashboard/schedule', 'DashboardController@schedule')->name('dashboard.schedule');
    Route::get('dashboard/currently-running', 'DashboardController@currentlyRunning');
    Route::get('dashboard/most-recent', 'DashboardController@mostRecent');
    Route::get('dashboard/upcoming', 'DashboardController@upcoming');

    /* notification-settings routes*/
    Route::apiResource('notification-settings', 'NotificationSettingsController');
});
